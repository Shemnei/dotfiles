# Create a new directory and enter it
mkd() {
    mkdir -p "$@" && cd "$@"
}

# Colorizes man output
man() {
	env \
		LESS_TERMCAP_md=$(tput bold; tput setaf 4) \
		LESS_TERMCAP_me=$(tput sgr0) \
		LESS_TERMCAP_mb=$(tput blink) \
		LESS_TERMCAP_us=$(tput setaf 2) \
		LESS_TERMCAP_ue=$(tput sgr0) \
		LESS_TERMCAP_so=$(tput smso) \
		LESS_TERMCAP_se=$(tput rmso) \
		PAGER="${commands[less]:-$PAGER}" \
		man "$@"
}

# Print README file
readme() {
    for readme in {readme,README}.{md,MD,markdown,txt,TXT,mkd}; do
        if [[ -f "$readme" ]]; then
            cat "$readme"
        fi
    done
}

# Git commit browser
gitlog() {
    local commit_hash="echo {} | grep -o '[a-f0-9]\{7\}' | head -1"
    local view_commit="$commit_hash | xargs -I % sh -c 'git show --color=always % | diff-so-fancy'"
    git log --color=always \
        --format="%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset" "$@" | \
    fzf --no-sort --tiebreak=index --no-multi --reverse --ansi \
        --header="enter to view, alt-y to copy hash" --preview="$view_commit" \
        --bind="enter:execute:$view_commit | less -R" \
        --bind="alt-y:execute:$commit_hash | xclip -selection clipboard"
}

# Environment management
win2lin() {
    # convert an absolute windows path to an absolute linux path
    win_path=$1
    win_path=${win_path/C://c}
    win_path=${win_path//\\//}
    win_path="/mnt$win_path"
    echo "$win_path"
}

lin2win() {
    # convert a (possibly relative) linux path to an absolute windows path
    lin_path=$(readlink -f $1)
    lin_path=${lin_path//'\/mnt\/c'/'C:'}
    lin_path=${lin_path//'\/'/'\\'}
    echo "$lin_path"
}

# display path variables
paths() {
  ENV_VAR="PATH"
    result=$k1
  if [ -z $1 ]; then
    echo "Display PATH variable:"
  else
    ENV_VAR=$1
  fi
  ENV_VAR=${(P)ENV_VAR}
  echo -e ${ENV_VAR//:/\\n}
}

# Delete path of first occurance of key $1 from env variable $2
pathrm () {
  KEY=$1
  ENV_VAR="PATH"
  if [ $2 ]; then
    ENV_VAR=$2
  fi
  ENV_VAR_CONT=${(P)ENV_VAR}
  # echo "Removing path of first occurance of $KEY from $ENV_VAR !"
  NEW_ENV=$(echo $ENV_VAR_CONT | sed 's#:[^:]*'"$KEY"'[^:]*##')
  export $ENV_VAR=$NEW_ENV
}

# Like pathrm but delete all occurances
pathrmall () {
    KEY=$1
    ENV_VAR="PATH"
    if [ $2 ]; then
        ENV_VAR=$2
    fi
    ENV_VAR_CONT=${(P)ENV_VAR}
    if [[ $ENV_VAR_CONT == *$KEY* ]]; then
        pathrm $1 $2
        pathrmall $1 $2
    fi
}

# update dotfiles
dotfiles() {
    # discards unstages changes
    git -C ~/setup checkout -- .
    git -C ~/setup pull
    # convert files with windows line endings to linux
	find ~/setup -type f -print0 | xargs -0 dos2unix
	chmod +x ~/setup/linux/ubuntu/dotfiles.sh
	~/setup/linux/ubuntu/dotfiles.sh
	exec zsh
}

# restart kubernetes pods by deployment
# usage: kuberestart deployment_name namespace [replicas]
kuberestart () {
    REPLICAS=1
    if [ $3 ]; then
        REPLICAS=$3
    fi
    kubectl scale deployment $1 --replicas=0 -n $2 && kubectl scale deployment $1 --replicas=$REPLICAS -n $2
}

# change default namespace
# usage: kubens namespace
kubens () {
    kubectl config set-context --current --namespace=$1
}
