# Default terminal
set-option -g default-terminal "screen-256color"

# Turn the mouse on
set-option -g mouse on

# Time in milliseconds for which tmux waits after an escape
set-option -g escape-time 0

# Enable supported focus events
set-option -g focus-events on

# History settings
set-option -g history-limit 10000
set-option -g history-file ~/.tmux/tmuxhistory

# Activity
set-option -g monitor-activity on
set-option -g visual-activity off

# Start window index at 1
set-option -g base-index 1

# Start pane index at 1
set-option -g pane-base-index 1

# Rename window to reflect current program
set-window-option -g automatic-rename on

# Renumber windows when one is closed
set-option -g renumber-windows on

# No bells at all
set-option -g bell-action none

# Add truecolor support
set-option -ga terminal-overrides ",xterm-256color:Tc"

# Default terminal is 256 colors
set -g default-terminal "screen-256color"

# Status position
set-option -g status-position top

# Status update interval
set-option -g status-interval 5

# Basic status bar colors
set-option -g status-bg default
set-option -g status-fg white

# Left side of status bar
set-option -g status-left-length 40
set-option -g status-left "#[fg=brightwhite,bg=brightblack] #S #[fg=default,bg=default] "

# Window status
set-option -g window-status-format "#[fg=white,bg=brightblack] #I #[fg=white,bg=#363636] #W "
set-option -g window-status-current-format "#[fg=brightwhite,bg=green] #I #[fg=brightwhite,bg=blue] #W "
set-option -g window-status-separator " "
set-option -g status-justify left

# Right side of status bar
set-option -g status-right-length 40
set-option -g status-right " #[fg=brightwhite,bg=#363636] %a, %d %b %H:%M #[fg=brightwhite,bg=brightblack] #(whoami)@#h "

# Pane number indicator
set-option -g display-panes-colour brightblack
set-option -g display-panes-active-colour brightwhite

# Clock mode
set-option -g clock-mode-colour white
set-option -g clock-mode-style 24

##  Plugins

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

# A tmux plugin to better manage and configure the mouse.
set -g @plugin 'nhdaly/tmux-better-mouse-mode'
set-option -g mouse on

# Tmux plugin for copying to system clipboard.
set -g @plugin 'tmux-plugins/tmux-yank'

# A plugin that enhances tmux search
set -g @plugin 'tmux-plugins/tmux-copycat'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
