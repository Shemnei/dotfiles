# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH

# required by gpg
export GPG_TTY=$(tty)

# display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Changing directories
DIRSTACKSIZE=8

setopt   AUTO_PUSHD         # Track history of directories for navigation
setopt   PUSHD_IGNORE_DUPS  # Keep dir stack clean of dupes
setopt   PUSHD_SILENT       # Otherwise popd prints the cd'd directory
unsetopt PUSHD_TO_HOME      # Ensure pushd swaps to most current dir and not $HOME

# Completion
setopt   AUTO_LIST
setopt   AUTO_MENU

unsetopt AUTO_NAME_DIRS    # Disable named dirs (ASDF=`pwd` would show ~ASDF in prompt)
setopt   COMPLETE_ALIASES  # Don't expand aliases _before_ completion has finished (like: git comm-<TAB>)
setopt   GLOB_COMPLETE     # Show menu with options matching pattern (like: cd D*s<TAB>)
unsetopt LIST_BEEP         # Shhh

# Globbing
setopt   MARK_DIRS          # Append a trailing '/' to all directory names resulting from globbing
setopt   GLOBDOTS           # Match also file names starting with dot
setopt   EXTENDED_GLOB      # Allow extended globbing, such as `ls (^.git/)#` to glob all files not in `.git`

# History
HISTFILE=~/.zhistory
HIST_STAMPS="yyyy-mm-dd"
HISTSIZE=10000
SAVEHIST=$HISTSIZE

setopt   EXTENDED_HISTORY      # Add command line timestamps to history
unsetopt HIST_BEEP             # no hist beep
setopt   HIST_FCNTL_LOCK       # Prevent history corruption
setopt   HIST_IGNORE_ALL_DUPS  # Keep history clean from dupes
setopt   HIST_IGNORE_SPACE     # Do not record command lines starting with space
setopt   HIST_NO_FUNCTIONS     # Do not record function definitions
setopt   HIST_REDUCE_BLANKS    # Remove superfluous blanks from each command line
setopt   HIST_SAVE_NO_DUPS     # Ensure no dupes when writing out to history file
setopt   HIST_VERIFY           # Do not execute lines directly on history expansion
setopt   INC_APPEND_HISTORY    # Write to $HISTFILE immediately
setopt   SHAREHISTORY          # Share history across terminals

# I/O
setopt   CORRECT               # Try to correct spelling of commands
#setopt   IGNORE_EOF            # Do not exit shell on <C-D>
setopt   INTERACTIVE_COMMENTS  # Allow comments in interactive shell

# Define end and home keys
bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line

# Misc
setopt NO_BEEP # disable terminal beep

# Init zinit
source $(brew --prefix)/opt/zinit/zinit.zsh

# Load starship theme
zinit ice as"command" from"gh-r" \
          atclone"./starship init zsh > init.zsh; ./starship completions zsh > _starship" \
          atpull"%atclone" src"init.zsh"
zinit light starship/starship
zinit wait lucid for \
 atinit"ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay" \
    zdharma/fast-syntax-highlighting \
 blockf \
    zsh-users/zsh-completions \
 atload"!_zsh_autosuggest_start" \
    zsh-users/zsh-autosuggestions

# Autocomplete init
zstyle :compinstall filename '$HOME/.zshrc'
autoload -Uz compinit
compinit

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    export PATH="$HOME/bin:$PATH"
fi
# also do this for .local (pip stuff needs it)
if [ -d "$HOME/.local/bin" ] ; then
    export PATH="$HOME/.local/bin:$PATH"
fi

# Load alias definitions
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Load functions
if [ -f ~/.bash_functions ]; then
    . ~/.bash_functions
fi

# start ssh agent using keychain
/usr/bin/keychain $HOME/.ssh/id_rsa --quiet
source $HOME/.keychain/$HOSTNAME-sh

# load autojump
[ -f /home/linuxbrew/.linuxbrew/etc/profile.d/autojump.sh ] && . /home/linuxbrew/.linuxbrew/etc/profile.d/autojump.sh

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# kubectl autocompletion
source <(kubectl completion zsh)

# fast node manager
eval "$(fnm env)"

# remove windows python paths to not disturb pipenv
pathrmall /mnt/c/Users/"$WIN_USER"/AppData/Local/Programs/Python
