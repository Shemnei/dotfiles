# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Load environment varaibles
if [ -f ~/.bash_env ]; then
    . ~/.bash_env
fi

# start homebrew (before zsh if zsh installed via homebrew)
eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)

if test -t 1; then
    exec zsh
fi
