# Bookmarks
alias dl='cd ~/downloads && ls -lA'
alias junk='cd ~/junk && ls -lA'
alias tmp='cd ~/temporary && ls -lA'
alias dev='cd ~/development && ls -lA'
alias winhome='cd ~/winhome && ls -lA'

# Tools
alias p='ps axo pid,user,pcpu,comm' # process overview
alias pg='ps aux | grep' # find a process
alias spider='wget --no-config --spider' # use wget as spider (check out website without downloading)
alias preview='fzf --height=50% --layout=reverse --preview="bat --color=always {}"' # preview current dir
alias incognito='unset HISTFILE' # do not save history in current session anymore
alias ch='echo > ~/.bash_history && echo > ~/.zsh_history' # empty history
alias ech='unset HISTFILE && exit' # quit bash shell without saving history
alias hgrep='history | grep' # find a command in the history
alias lgrep='ls -l | grep' # find a file in the current dir
alias lagrep='ls -lA | grep' # find a hidden file in the current dir
alias fdir='find . -type d -name' # find a dir by name in current dir
alias ff='find . -type f -name' # find a file by name in current dir
alias timer='echo "Timer started. Stop with Ctrl-D." && date "+%a, %d %b %H:%M:%S" && time cat && date "+%a, %d %b %H:%M:%S"' # starts a stopwatch
alias tolf='find . -type f -print0 | xargs -0 dos2unix' # convert all files from current directory recursivley into LF
alias gitdiscard='git clean -df && git checkout -- .' # discards unstages changes
alias telnet='curl -v telnet://' # use curl to telnet
alias upgrade='sudo apt-get update && sudo apt-get upgrade -y' # upgrade all packages
alias unlock='ssh-add ~/.ssh/id_rsa'

# Aliases
alias disk='df'
alias vdisk='ncdu'
alias search='fzf'
alias files='fzf'
alias :q='exit'
alias help='man'
alias quit='exit'

# Shortcuts
alias f='fzf'
alias c='clear'
alias e='exit'
alias h='history'

# Improve existing commands
alias cp='cp -iv'
alias mv='mv -iv'
alias ln='ln -iv'
alias mkdir='mkdir -v'
alias rm='rm -i'
alias rmf='rm -rf'
alias uptime='uptime -p'
alias free='free -h'
alias df='df -h'

# Enable colors
alias dir='dir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias pgrep='pgrep --color=auto'

# Navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'
alias ~='cd ~'
alias -- -='cd -'

# LS
alias ll='ls -alF --color=auto'
alias la='ls -A --color=auto'
alias ls='ls -lCFh --group-directories-first --color=auto'

# Git
alias g='git'
alias gd='gitdiscard'
alias gc='git commit'
alias gcus='git -c commit.gpgsign=false commit'
alias gcm='git commit -m'
alias gp='git push'
alias ga='git add'
alias gaa='git add .'

# Kubectl
alias k='kubectl'
alias kubelog='kubectl logs --tail 50 --follow '
alias kubewatch='kubelog'
alias kubestate='watch kubectl get pods '
alias kubectx='kubectl config use-context '

# Windows
alias docker='docker.exe'
alias firefox='"/mnt/c/Program Files/Mozilla Firefox/firefox.exe"'
alias powershell='powershell.exe'
alias ps='powershell'

# Enable aliases in watch (https://unix.stackexchange.com/questions/25327/watch-command-alias-expansion)
alias watch='watch --color '
