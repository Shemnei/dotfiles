# michidk's dotfiles

This repository contains my [dotfiles](https://wiki.archlinux.org/title/Dotfiles) that are compiled with [punktf](https://github.com/Shemnei/punktf).

## Setup

To deploy the dotfiles [punktf](https://github.com/Shemnei/punktf) is required.

## Deployment

```bash
# Test profile
punktf -vv deploy linux_arch_mtx-arch --dry-run

# Deploy profile
punktf deploy linux_arch_mtx-arch
```

